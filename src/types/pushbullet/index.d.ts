/**
 * Push Bullet
 */

declare module 'pushbullet' {
  import { EventEmitter } from 'events';

  export type callback<T = void> = (err?: Error | null, result?: T) => void;

  export interface ConstructorOpts {
    fullResponses?: boolean;
  }

  interface Base {
    active: boolean;
    created: number;
    iden: string;
    modified: number;
  }

  export interface Account {}

  export interface Block {}

  export interface Channel {}

  export interface Chat {}

  export interface Client {}

  export interface Contact {}

  export interface Device extends Base {
    app_version: number;
    icon: string;
    manufacturer: string;
    model: string;
    nickname: string;
    push_token: string;
  }

  export interface DeviceOptions {
    active?: boolean;
    cursor?: string;
    limit?: number;
  }

  export interface Grant {}

  export interface PushBulletOutput {
    accounts: Account[];
    blocks: Block[];
    channels: Channel[];
    chats: Chat[];
    clients: Client[];
    contacts: Contact[];
    cursor: string;
    devices: Device[];
    grants: Grant[];
    profiles: Profile[];
    pushes: Push[];
    subscriptions: Subscription[];
    texts: Text[];
  }

  export interface Profile {}

  export interface Push extends Base {
    await_app_guids?: string[];
    body: string;
    client_iden?: string;
    channel_iden?: string;
    direction: 'self' | 'outgoing' | 'incoming';
    dismissed: boolean;
    file_name?: string;
    file_type?: string;
    file_url?: string;
    guid: string;
    image_url?: string;
    image_width?: number;
    image_height?: number;
    receiver_email: string;
    receiver_email_normalized: string;
    receiver_iden: string;
    sender_email: string;
    sender_email_normalized: string;
    sender_iden: string;
    sender_name: string;
    source_device_iden?: string;
    title: string;
    target_device_iden?: string;
    type: 'note' | 'file' | 'link';
    url?: string;
  }

  export interface PushOptions {
    active?: boolean;
    cursor?: string;
    limit?: number;
    modified_after?: number;
  }

  export interface Stream extends EventEmitter {
    connect() : void;
    close() : void;
  }

  export interface Subscription{}

  export interface Text {}

  export interface User extends Base {
    email: string;
    email_normalized: string;
    image_url: string;
    max_upload_size: number;
    name: string;
  }

  export default class PushBullet {
    constructor(url: string, opts?: ConstructorOpts);

    createDevice(opts: Partial<Device>) : Promise<Device>;
    createDevice(opts: Partial<Device>, cb: callback<Device>) : void;

    devices(opts?: DeviceOptions) : Promise<PushBulletOutput>;
    devices(cb: callback<PushBulletOutput>) : void;
    devices(opts: DeviceOptions, cb: callback<PushBulletOutput>) : void;

    dismissPush(iden: string): Promise<Push>;
    dismissPush(iden: string, cb: callback<Push>): void;

    enableEncryption(password: string, userIden: string) : void;

    history(opts?: PushOptions) : Promise<PushBulletOutput>;
    history(cb: callback<PushBulletOutput>) : void;
    history(opts: PushOptions, cb: callback<PushBulletOutput>) : void;

    note(iden: string, title: string, body: string) : Promise<void>;
    note(iden: string, title: string, body: string, cb: callback<void>) : void;

    me() : Promise<User>;
    me(cb: callback<User>) : void;

    stream() : Stream;
  }
}
