/**
 * mongodb
 */

/* Node modules */
import { EventEmitter } from 'events';

/* Third-party modules */
import mongoose from 'mongoose';

/* Files */
import { IDatabase } from '../interfaces/db';
import { ILogger } from '../interfaces/logger';
import { IMongooseOpts } from '../interfaces/queue';

export default class MongoDB extends EventEmitter implements IDatabase {

  protected logger: ILogger;

  constructor(logger: ILogger, protected opts: IMongooseOpts) {
    super();

    this.logger = logger.child({
      db: 'MongoDB',
    });

    this.configureMongoose();
  }

  configureMongoose() {
    mongoose.connection
      .on('disconnected', () => {
        this.logger.event('fatal', 'MongoDB disconnected');

        this.emit('error', new Error('MongoDB disconnected'));
      });

    mongoose.set('debug', (collection: string, method: string, query: any, ...args: any[]) => {
      this.logger.event('debug', 'New query', {
        collection,
        method,
        query,
        args,
      });
    });
  }

  /**
   * Connect
   *
   * Handle the connection to MongoDB
   */
  async connect() : Promise<void> {
    this.logger.event('info', 'Connecting');

    await mongoose.connect(this.opts.url, this.opts.opts);

    this.logger.event('info', 'Connected to database');
  }

  /**
   * Create DB Structure
   *
   * As this is MongoDB, this does nothing
   */
  async createDbStructure(): Promise<void> {
    this.logger.event('info', 'Create DB structure');
  }

}
