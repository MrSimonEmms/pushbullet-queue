/**
 * pushBullet
 */

/* Node modules */
import { EventEmitter } from 'events';

/* Third-party modules */
import PB, { PushOptions, Stream } from 'pushbullet';
import { ILogger } from '../interfaces/logger';

/* Files */
import Message from '../model/message';
import { IPushBulletOpts } from '../interfaces/queue';

export default class PushBullet extends EventEmitter {

  protected logger: ILogger;
  public pb: PB;
  protected stream: Stream;

  constructor(logger: ILogger, protected token: string, protected opts: IPushBulletOpts = {}) {
    super();

    if (!this.opts.processAttempts) { this.opts.processAttempts = 3; }

    this.logger = logger.child({
      queue: 'PushBullet',
    });
    this.pb = new PB(token);
    this.stream = this.pb.stream();
  }

  async dismissPush(iden: string) : Promise<void> {
    await this.pb.dismissPush(iden);
  }

  /**
   * Listen
   *
   * Connect to the PushBullet stream and listen for
   * new messages
   */
  listen() : PushBullet {
    this.logger.event('info', 'Listening for events on PushBullet');

    /* Log useful events */
    this.stream
      .on('tickle', async (...args: any[]) => {
        this.logger.event('info', 'New tickle received, searching for pushes', {
          args,
        });

        await this.sync();
      })
      .on('connect', () => {
        this.logger.event('info', 'Listening for changes');
      })
      .on('close', () => {
        this.logger.event('warn', 'Closed');

        if (this.opts.errorOnClose !== false) {
          this.emit('error', new Error('PushBullet connection closed'));
        }
      })
      .on('nop', () => {
        this.logger.event('debug', 'Received heartbeat');
      })
      .on('error', (err) => {
        this.logger.event('error', 'Error', {
          err,
        });

        /* Preserve the error */
        this.emit('error', err);
      })
      .connect();

    return this;
  }

  async note(iden: string, title: string, body: string) : Promise<void> {
    await this.pb.note(iden, title, body);
  }

  /**
   * Sync
   *
   * Synchronise PushBullet events with our database
   */
  async sync() : Promise<void> {
    const since = await Message.getLatestMessage();
    const opts : PushOptions = {};

    if (since) {
      opts.modified_after = since.messageUpdatedAt.getTime() / 1000;
    }

    this.logger.event('info', 'Synchronising messages', {
      opts,
    });

    const { pushes } = await this.pb.history(opts);

    this.logger.event('debug', 'Retrieved list of messages', {
      pushes,
    });

    this.logger.event('info', 'Retrieved messages', {
      pushCount: pushes.length,
    });

    await Promise.all(pushes.filter(push => push.active)
      .map(async (data) => {
        let msg = await Message.getByQueueId(data.iden);

        if (msg) {
          this.logger.event('info', 'Updating message');

          msg.set('ack', data.dismissed);
        } else {
          msg = new Message({
            data,
            ack: data.dismissed,
            queueMessageId: data.iden,
            receiverId: data.target_device_iden || data.receiver_iden,
            senderId: data.sender_iden,
            messageCreatedAt: new Date(Math.ceil(data.created * 1000)),
            messageUpdatedAt: new Date(Math.ceil(data.modified * 1000)),
          });

          this.logger.event('info', 'Creating new message', {
            id: data.iden,
          });

          this.logger.event('debug', 'Creating message from PushBullet data', {
            data,
            msg,
          });
        }

        await msg.save();
      }));

    this.logger.event('info', 'Retrieving unacknowledged messages from database');

    const messages = await Message.getUnackedMessages(this.opts.processAttempts!);

    this.emit('messages', messages);
  }

}
