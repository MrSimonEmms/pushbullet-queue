/**
 * queue
 */

/* Node modules */
import { EventEmitter } from 'events';

/* Third-party modules */
import BetterQueue from 'better-queue';
import uuid from 'uuid';

/* Files */
import Logger from './logger';
import MongoDB from './mongodb';
import PushBullet from './pushBullet';
import Callback from './callback';
import { IDatabase } from '../interfaces/db';
import { IFactoryOpts, ILoggerOpts, IQueryOpts, ITask, ITaskFn } from '../interfaces/queue';
import { ILogger } from '../interfaces/logger';
import { IMessage } from '../interfaces/message';

export default class Queue extends EventEmitter {

  public db: IDatabase;
  public logger: ILogger;
  public pb: PushBullet;
  protected queue: BetterQueue<ITaskFn, void>;
  protected callbackServer: Callback;
  protected tasks: ITask[];

  get pbInst () {
    return this.pb.pb;
  }

  constructor(pushBulletToken: string, protected opts: IFactoryOpts) {
    super();

    this.logger = this.configureLogger();
    this.db = this.configureDb();
    this.pb = this.configurePushBullet(pushBulletToken);
    this.tasks = this.configureTasks();
    this.queue = this.configureQueue(opts);
    this.callbackServer = new Callback(opts, this.logger);

    this.logger.event('info', 'PushBullet Queue created', opts);
  }

  /**
   * Configure DB
   *
   * This configures the database object
   *
   * @todo add support for Sequelize DBs
   */
  protected configureDb() : IDatabase {
    let db : IDatabase;
    if (this.opts.db.type === 'mongodb') {
      db = new MongoDB(this.logger, this.opts.db);
    } else {
      this.logger.event('fatal', 'Unsupported database type', {
        type: this.opts.db.type,
      });

      throw new Error('Unsupported database type');
    }

    return this.registerPassThru<IDatabase>(db, [
      'error',
    ]);
  }

  protected configureLogger() : ILogger {
    /* Always redact tokens in config */
    const redactTokens = [
      'db.url',
    ];

    const loggerOpts: ILoggerOpts = this.opts.logger || {};
    if (!Array.isArray(loggerOpts.redact)) {
      loggerOpts.redact = [];
    }

    redactTokens.forEach((redact) => {
      if (!loggerOpts.redact!.includes(redact)) {
        loggerOpts.redact!.push(redact);
      }
    });

    return this.registerPassThru<ILogger>(Logger.create(loggerOpts), [
      'log',
    ]);
  }

  protected configurePushBullet(token: string) : PushBullet {
    const pb = new PushBullet(this.logger, token, this.opts.pushBullet);

    pb.on('messages', async (messages: IMessage[]) => {
      this.logger.event('info', 'New messages received', {
        count: messages.length,
      });
      this.logger.event('debug', 'New message detail', messages);

      messages.forEach((message) => {
        const receiverId = message.get('receiverId');
        const id = message.get('id');
        const pushData = message.get('data');

        let hasTask : boolean = false;
        this.tasks.forEach((task) => {
          const taskName = task.name;
          const isAsync = task.async;

          if (task.deviceId === receiverId) {
            /* There's a task */
            hasTask = true;

            const taskLogger = this.logger.child({
              id,
              taskName,
            });

            this.queue.push({
              message,
              fn: async () => {

                try {
                  await message.lockMessage();

                  const opts : IQueryOpts = {
                    logger: taskLogger,
                  };

                  if (isAsync) {
                    const { domain } = this.callbackServer;

                    if (domain) {
                      opts.asyncUrl = `${domain}${Callback.urls.async}/${id}`;
                    } else {
                      taskLogger.event('warn', 'Async task but server not configured');
                    }
                  }

                  await task.task(pushData, opts);

                  taskLogger.event('info', 'Acking message in PushBullet');

                  await this.pb.dismissPush(message.queueMessageId);

                  if (isAsync) {
                    taskLogger.event('info', 'Task is asynchronous - awaiting HTTP call');
                    return;
                  }

                  taskLogger.event('info', 'Acking message in DB');

                  await message.unlockMessage();
                  await message.ackMessage();
                } catch (err) {
                  taskLogger.event('error', 'Runner error', {
                    err,
                  });

                  await message.unlockMessage();

                  throw err;
                }
              },
            }).on('failed', async (err: Error) => {
              taskLogger.event('error', 'Task failed, not trying again', {
                err,
              });

              /* Alert the error */
              let iden = pushData.source_device_iden;
              if (this.opts.pushBullet) {
                const { alert } = this.opts.pushBullet;

                if (alert === true) {
                  /* Ignore - use the source device iden */
                } else if (alert === false) {
                  /* No alert - ignore */
                  taskLogger.event('debug', 'Pushing of error suppressed');
                  return;
                } else {
                  /* Send to the specified iden rather than the sender */
                  iden = alert;
                }
              }

              taskLogger.event('info', 'Publishing error', {
                iden,
              });

              await this.pb.note(iden, 'Task error', `Task ID: ${id}
Task name: ${taskName}
PushBullet ID: ${iden}
Sender: ${pushData.sender_name}
Title: ${pushData.title}

Error stack:
${err.stack || err}`);
            });
          }
        });

        if (!hasTask) {
          this.queue.push({
            message,
            fn: async () => {
              this.logger.event('info', 'Push has no corresponding task - acking locally', {
                id,
              });

              await message.ackMessage();
            },
          });
        }
      });
    });

    return this.registerPassThru<PushBullet>(pb, [
      'error',
    ]);
  }

  protected configureQueue(opts: IFactoryOpts) : BetterQueue {
    let maxRetries = 3;
    if (opts.pushBullet && opts.pushBullet.processAttempts) {
      maxRetries = opts.pushBullet.processAttempts;
    }

    return new BetterQueue(
      async ({ message, fn }: ITaskFn, cb) => {
        try {
          const id = message.get('id');
          const attempt = await message.addProcessAttempt();

          this.logger.event('info', 'Processing task', {
            id,
            attempt,
          });

          const start = Date.now();
          await fn();

          this.logger.event('info', 'Task completed', {
            id,
            attempt,
            duration: Date.now() - start,
          });

          cb(null);
        } catch (err) {
          this.logger.event('error', 'Task failed', {
            err,
          });

          cb(err);
        }
      },
      {
        maxRetries,
        id: 'queueMessageId',
        concurrent: 1,
        filo: false,
        preconditionRetryTimeout: 100,
        retryDelay: 1000,
      },
    );
  }

  protected configureTasks() : ITask[] {
    const names : string[] = [];

    return this.opts.tasks
      .map((task: ITask) : ITask => {
        const name = task.name || uuid.v4();

        if (names.includes(name)) {
          this.logger.event('fatal', 'Task name already registered', {
            task,
          });

          throw new Error(`Task name already registered: ${name}`);
        }

        names.push(name);

        return {
          name,
          async: task.async || false,
          deviceId: task.deviceId,
          task: task.task,
        };
      });
  }

  protected async connectDb() : Promise<void> {
    try {
      this.logger.event('trace', 'Configuring database connection');

      await this.db.createDbStructure();
      await this.db.connect();

      this.logger.event('trace', 'Database configured and connected successfully');
    } catch (err) {
      this.logger.event('fatal', 'Failed to configure and connect the database', {
        err,
      });

      this.emit('error', err);
    }
  }

  protected registerPassThru<T extends EventEmitter>(listener: T, events: string[]) : T {
    return events.reduce(
      (result: T, event: string) : T => result
        .on(event, (...args: any[]) => this
          .emit(event, ...args)),
      listener,
    );
  }

  public replaceTasks(tasks: ITask[]) : ITask[] {
    this.logger.event('info', 'Replacing all tasks');
    this.opts.tasks = tasks;

    this.tasks = this.configureTasks();

    return this.tasks;
  }

  public async start() {
    /* First check the database */
    await this.connectDb();

    await this.callbackServer.start();

    /* Set up listener and get unacknowledged messages */
    this.pb.listen()
      .sync();
  }

}
