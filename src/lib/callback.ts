/**
 * server
 */

/* Node modules */

/* Third-party modules */
import express from 'express';
import uuid from 'uuid';

/* Files */
import Express, { IRequest } from '../interfaces/server';
import { IFactoryOpts, IServerOpts } from '../interfaces/queue';
import { ILogger } from '../interfaces/logger';
import Message from '../model/message';

export default class Callback {

  protected app: Express;
  protected opts: IServerOpts = {
    domain: 'http://localhost:3000',
    port: 3000,
  };

  constructor(opts: IFactoryOpts, protected logger: ILogger) {
    if (opts.server) {
      /* Maintain JS compatibility */
      if (opts.server.port) {
        /* Set default port - update domain to use port */
        this.opts.port = opts.server.port;
        this.opts.domain = `http://localhost:${this.opts.port}`;
      }

      if (opts.server.domain) {
        /* Set domain */
        this.opts.domain = opts.server.domain;
      }
    }

    this.app = express();

    this.app.set('x-powered-by', undefined);

    this.configureEndpoints();
  }

  get domain () : string | void {
    if (this.opts) {
      return this.opts.domain;
    }
  }

  protected configureEndpoints() : Callback {
    this.app.use((req: IRequest, _res: express.Response, next: express.NextFunction) => {
      req.log = this.logger.child({
        httpId: uuid.v4(),
      });

      req.log.event('debug', 'New HTTP call', {
        req,
      });

      next();
    });

    this.app.all(`${Callback.urls.async}/:id`, async (req: IRequest, res: express.Response) => {
      const { id } = req.params;
      const logger = req.log!.child({
        messageId: id,
      });

      try {
        const msg = await Message.getUnfinishedAsyncMessage(id);

        if (!msg) {
          logger.event('warn', 'Cannot find message');

          res.sendStatus(404);
        } else {
          logger.event('info', 'Acking message in DB');

          await msg.unlockMessage();
          await msg.ackMessage();

          res.send({
            id,
            status: 'completed',
          });
        }
      } catch (err) {
        logger.event('error', 'Unable to parse callback', {
          err,
        });

        res.status(400)
          .send({
            id,
            err: err.message,
            status: 'error',
          });
      }

      logger.event('debug', 'HTTP call resolved', {
        res,
      });
    });

    return this;
  }

  async start() : Promise<void> {
    await new Promise((resolve) => {
      this.app.listen(this.opts.port, () => {
        resolve();
      });
    });
  }

  static get urls() : { [key: string] : string } {
    return {
      async: '/async-callback',
    };
  }

}
