/**
 * logger
 */

/* Node modules */
import { EventEmitter } from 'events';

/* Third-party modules */
import pino, { Level, Logger as PinoLogger } from 'pino';

/* Files */
import { IKeyValue , ILoggerFn, ILogger, ILoggerChildBindings } from '../interfaces/logger';
import { ILoggerOpts } from '../interfaces/queue';

export default class Logger extends EventEmitter implements ILogger {

  protected constructor(public pino: PinoLogger) {
    super();
  }

  child(bindings: ILoggerChildBindings): ILogger {
    return new Logger(this.pino.child(bindings));
  }

  event(level: Level, msg: string, obj: IKeyValue<any> = {}, ...args: any[]): void {
    Logger.factory(level, this.pino)(msg, obj, ...args);

    /* Emit on nextTick so makes listeners asynchronous */
    process.nextTick(() => {
      this.emit('log', level, msg, obj, ...args);
    });
  }

  static create(config: ILoggerOpts = {}) {
    /* Allow injection of a Pino instance */
    let inst: PinoLogger;

    if (config.logger) {
      inst = config.logger;
    } else {
      config.serializers = pino.stdSerializers;

      inst = pino(config);
    }

    return new Logger(inst);
  }

  protected static factory(level: string, logger: PinoLogger) : ILoggerFn {
    return (msg: string, obj: IKeyValue<any> = {}, ...args: any[]) : void => {
      logger[level](obj, msg, ...args);
    };
  }

}
