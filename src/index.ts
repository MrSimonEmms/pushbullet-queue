/**
 * PushBullet Queue
 *
 * Make a simple message queue driven task runner using PushBullet
 *
 * @link https://gitlab.com/riggerthegeek/pushbullet-queue
 * @license MIT
 * @author Simon Emms <simon@simonemms.com>
 */

/* Node modules */

/* Third-party modules */
import PushBullet from 'pushbullet';

/* Files */
import { IFactoryOpts } from './interfaces/queue';
import Queue from './lib/queue';

export {
  PushBullet,
};

/* Export the class constructor */
export default async (pushBulletToken: string, opts: IFactoryOpts) : Promise<Queue> => {
  const queue = new Queue(pushBulletToken, opts);

  await queue.start();

  return queue;
};
