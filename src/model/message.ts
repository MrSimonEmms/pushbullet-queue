/**
 * message
 */

/* Node modules */

/* Third-party modules */
import { model, Schema, Types } from 'mongoose';

/* Files */
import { IMessage, IMessageModel } from '../interfaces/message';

export const modelName = 'Message';

const schema : Schema = new Schema({
  ack: {
    type: Boolean,
    default: false,
  },
  data: {
    required: true,
    type: Object,
  },
  receiverId: {
    required: true,
    type: String,
  },
  senderId: {
    required: true,
    type: String,
  },
  queueMessageId: {
    required: true,
    type: String,
    unique: true,
  },
  messageCreatedAt: {
    required: true,
    type: Date,
  },
  messageUpdatedAt: {
    required: true,
    type: Date,
    index: true,
  },
  lock: {
    type: Boolean,
    default: false,
  },
  processAttempts: {
    type: Number,
    default: 0,
  },
  createdAt:  {
    type: Date,
  },
  updatedAt: {
    type: Date,
  },
});

schema.index({
  ack: 1,
  processAttempts: -1,
});

schema.pre('save', function () {
  const now = new Date();
  if (!this.get('createdAt')) {
    this.set('createdAt', now);
  }
  this.set('updatedAt', now);
});

schema.methods.ackMessage = async function () : Promise<void> {
  this.set('ack', true);

  await this.save();
};

schema.methods.addProcessAttempt = async function () : Promise<number> {
  const nextAttempt : number = this.get('processAttempts') + 1;
  this.set('processAttempts', nextAttempt);

  await this.save();

  return nextAttempt;
};

schema.methods.lockMessage = async function () : Promise<void> {
  this.set('lock', true);

  await this.save();
};

schema.methods.unlockMessage = async function () : Promise<void> {
  this.set('lock', false);

  await this.save();
};

schema.statics.getByQueueId = async function (queueMessageId: string) : Promise<IMessage | null> {
  return Message.findOne({
    queueMessageId,
  });
};

/**
 * Get Latest Message
 *
 * Return the most recent message in the database.
 *
 * @return {Promise<IMessage | null>}
 */
schema.statics.getLatestMessage = async function () : Promise<IMessage | null> {
  return Message.findOne()
    .sort({
      messageUpdatedAt: 'desc',
    });
};

/**
 * Get Unacked Messages
 *
 * Retrieve all the unacknowledged messages, with the oldest
 * message first
 *
 * @returns {Promise<IMessage[]>}
 */
schema.statics.getUnackedMessages = async function (
  maxAttempts: number,
  unlockedOnly : boolean = true,
) : Promise<IMessage[]> {
  const conditions: any = {
    ack: false,
  };

  if (unlockedOnly) {
    /* Only retrieve unlocked messages */
    conditions.lock = false;
  }

  if (maxAttempts > 0) {
    /* Ensure we've not exceeded the number of attempts */
    conditions.processAttempts = {
      $lte: maxAttempts,
    };
  }

  return Message.find(conditions).sort({
    messageCreatedAt: 'asc',
  });
};

/**
 * Get Unfinished Async Message
 *
 * Retrieves a message by ID and locked status
 *
 * @param {string} id
 * @returns {Promise<IMessage | null>}
 */
schema.statics.getUnfinishedAsyncMessage = async function (id: string) : Promise<IMessage | null> {
  return Message.findOne({
    _id: new Types.ObjectId(id),
    lock: true,
  });
};

const Message: IMessageModel = model<IMessage, IMessageModel>(modelName, schema);

export default Message;
