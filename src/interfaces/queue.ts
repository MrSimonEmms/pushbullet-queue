/**
 * queue
 */

/* Node modules */

/* Third-party modules */
import { ConnectionOptions } from 'mongoose';
import { Logger, SerializerFn } from 'pino';

/* Files */
import { ILogger } from './logger';
import { IMessage } from './message';
import { IPush } from './pushBullet';

export interface IFactoryOpts {
  db: IMongooseOpts; // @todo add support for Sequelize DBs
  logger?: ILoggerOpts;
  pushBullet?: IPushBulletOpts;
  server?: IServerOpts;
  tasks: ITask[];
}

export interface ILoggerOpts {
  logger?: Logger;
  level?: string;
  name?: string;
  redact?: string[];
  serializers?: { [key: string]: SerializerFn };
}

export interface IMongooseOpts {
  opts?: ConnectionOptions;
  type: 'mongodb';
  url: string;
}

export interface IPushBulletOpts {
  alert?: string | boolean;
  errorOnClose?: boolean;
  processAttempts?: number;
}

export interface IQueryOpts {
  asyncUrl?: string;
  logger: ILogger;
}

export interface IServerOpts {
  domain: string;
  port: number;
}

export interface ITask {
  deviceId: string;
  name: string;
  task(msg: IPush, opts?: IQueryOpts) : Promise<void>;
  async?: boolean;
}

export interface ITaskFn {
  fn() : Promise<void> | void;
  message: IMessage;
}
