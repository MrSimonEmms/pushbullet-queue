/**
 * message
 */

/* Node modules */

/* Third-party modules */
import { Document, Model } from 'mongoose';

/* Files */

/* Model definition */
export interface IMessageDocument extends Document {
  ack: boolean;
  data: {
    [key: string]: any;
  };
  receiverId: string;
  senderId: string;
  queueMessageId: string;
  messageCreatedAt: Date;
  messageUpdatedAt: Date;
  readonly createdAt: Date;
  readonly updatedAt: Date;
}

/* Model methods */
export interface IMessage extends IMessageDocument {
  ackMessage() : Promise<void>;
  addProcessAttempt() : Promise<number>;
  lockMessage() : Promise<void>;
  unlockMessage() : Promise<void>;
}

/* Static methods */
export interface IMessageModel extends Model<IMessage> {
  getByQueueId(id: string) : Promise<IMessage | null>;
  getLatestMessage() : Promise<IMessage | null>;
  getUnackedMessages(maxAttempts: number) : Promise<IMessage[]>;
  getUnfinishedAsyncMessage(id: string) : Promise<IMessage | null>;
}
