/**
 * db
 */

/* Node modules */
import { EventEmitter } from 'events';

/* Third-party modules */

/* Files */

export interface IDatabase extends EventEmitter {
  connect() : Promise<void>;
  createDbStructure() : Promise<void>;
}
