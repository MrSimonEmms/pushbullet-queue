/**
 * logger
 */

/* Node modules */
import { EventEmitter } from 'events';

/* Third-party modules */
import { BaseLogger, Level, SerializerFn } from 'pino';

/* Files */

export interface IKeyValue<V> {
  [key: string]: V;
}

export interface ILoggerFn {
  (msg: string, obj?: IKeyValue<any>, ...args: any[]) : void;
}

export interface ILoggerChildBindings {
  level?: Level | string;
  serializers?: {
    [key: string]: SerializerFn,
  };
  [key: string]: any;
}

export interface ILogger extends EventEmitter {
  readonly pino: BaseLogger;
  child(bindings: ILoggerChildBindings): ILogger;
  event(level: Level, msg: string, obj?: IKeyValue<any>, ...args: any[]) : void;
}
