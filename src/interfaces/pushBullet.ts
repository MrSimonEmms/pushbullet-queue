/**
 * pushBullet
 *
 * This is copy/pasta of the pushbullet module, in /src/types. This
 * is to allow use of this type in the library, which isn't possible
 * until the pushbullet module's types are in the public domain.
 */

/* Node modules */

/* Third-party modules */

/* Files */

export interface IPush {
  active: boolean;
  created: number;
  iden: string;
  modified: number;
  await_app_guids?: string[];
  body: string;
  client_iden?: string;
  channel_iden?: string;
  direction: 'self' | 'outgoing' | 'incoming';
  dismissed: boolean;
  file_name?: string;
  file_type?: string;
  file_url?: string;
  guid: string;
  image_url?: string;
  image_width?: number;
  image_height?: number;
  receiver_email: string;
  receiver_email_normalized: string;
  receiver_iden: string;
  sender_email: string;
  sender_email_normalized: string;
  sender_iden: string;
  sender_name: string;
  source_device_iden?: string;
  title: string;
  target_device_iden?: string;
  type: 'note' | 'file' | 'link';
  url?: string;
}
