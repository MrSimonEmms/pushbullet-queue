/**
 * server
 */

/* Node modules */

/* Third-party modules */
import express, { Express } from 'express';
import { ILogger } from './logger';

/* Files */

export default Express;

export interface IRequest extends express.Request {
  log?: ILogger;
}
