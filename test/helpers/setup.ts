/**
 * setup
 */

/* Node modules */

/* Third-party modules */
import chai, { expect } from 'chai';
import { noCallThru as proxyquire } from 'proxyquire';
import sinon from 'sinon';
import sinonChai from 'sinon-chai';

/* Files */

chai.use(sinonChai);

export {
  expect,
  proxyquire,
  sinon,
};
