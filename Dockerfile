FROM node:10-alpine
WORKDIR /opt/app
ADD . .
RUN npm i \
  && npm run compile
USER node
CMD [ "npm", "run", "serve" ]
