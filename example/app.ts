/**
 * app
 */

/* Node modules */

/* Third-party modules */

/* Files */
import queue from '../src';
import task1 from './tasks/task1';
import asyncTask1 from './tasks/asyncTask1';

(async () => {
  try {
    await queue(`${process.env.PUSHBULLET_TOKEN}`, {
      db: {
        type: 'mongodb',
        opts: {
          dbName: process.env.DB_MONGODB_DB_NAME,
          useCreateIndex: true,
          useNewUrlParser: true,
        },
        url: `${process.env.DB_MONGODB_URL}`,
      },
      logger: {
        level: process.env.LOGGER_LEVEL || 'info',
      },
      tasks: [{
        deviceId: `${process.env.PUSHBULLET_DEVICE_ID}`,
        name: 'task1',
        task: task1,
      }, {
        deviceId: `${process.env.PUSHBULLET_DEVICE_ID}`,
        name: 'asyncTask1',
        task: asyncTask1,
        async: true,
      }],
    });
  } catch (err) {
    console.log(err.stack);
    process.exit(1);
  }
})();
