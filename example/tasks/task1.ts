/**
 * task1
 */

/* Node modules */

/* Third-party modules */

/* Files */
import { IQueryOpts } from '../../src/interfaces/queue';
import { IPush } from '../../src/interfaces/pushBullet';

export default async (push: IPush, { logger }: IQueryOpts) : Promise<void> => {
  logger.pino.info({ push }, 'received message');

  await new Promise(resolve => setTimeout(resolve, 1000));

  logger.pino.info('completed message');
};
