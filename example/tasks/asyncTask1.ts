/**
 * task2
 */

/* Node modules */

/* Third-party modules */
import request from 'request-promise-native';

/* Files */
import { IQueryOpts } from '../../src/interfaces/queue';
import { IPush } from '../../src/interfaces/pushBullet';

export default async (push: IPush, { asyncUrl, logger }: IQueryOpts) : Promise<void> => {
  logger.event('info', 'received message', { push });

  await new Promise(resolve => setTimeout(resolve, 1000));

  logger.event('info', 'completed message - not unlocking as async task');

  /**
   * This emulates async task completion. This would normally be outside this script,
   * for example using an OpenFAAS Async function
   *
   * @link https://docs.openfaas.com/reference/async/
   */
  setTimeout(
    async () => {
      logger.event('info', 'releasing lock', {
        asyncUrl,
      });

      /* The server exists on all HTTP verbs */
      await request.get(asyncUrl!);
    },
    2000);
};
